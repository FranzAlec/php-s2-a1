<?php

    $products = [];

    function createProduct ($name, $price){

        global $products; //to get or access the global variable array inside this function
        //tells the createProduct function that we are going to use the global var $products and modify it on its block

        array_push($products,
        ['name' => $name, 'price' =>  $price]);
    }




    function printProducts(){

        global $products;

        foreach($products as $product){
            echo "<li>";
            foreach($product as $label => $value){
                echo "$label : $value, ";
            }
            echo "</li>";
        }
    }


    function countProducts ($productList){
        $count = count($productList);

        echo "Total number of products: $count";
    }


    function deleteProduct (){

        global $products;

        array_pop($products);
    }
?>