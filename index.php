<?php require "./code.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array Manipulation | Activity</title>
</head>
<body>
    <h1>Activity</h1>

    <pre>
        <?php 
            createProduct('jeans', 150.00);
            createProduct('shirt', 300.00);
            createProduct('shirt', 300.00);
            print_r($products);
        ?>
    </pre>
    <h2>List of Products</h2>
    <ul>
        <?php printProducts($products); ?>
    </ul>  

    <p>
        <?php countProducts($products); ?>
    </p>
    
    <pre>
        <?php 
        deleteProduct($products); 
        print_r($products);
        ?>
    </pre>

</body>
</html>